<?php

namespace UBB\DisciplineBundle\Entities;

use Doctrine\ORM\Mapping as ORM;
use UBB\ScheduleBundle\Entities\Repartition;

/**
 * Class Discipline
 * @package UBB\DisciplineBundle\Entities
 *
 * @ORM\Table(name="disc")
 */
class Discipline
{
    /**
     * @var int $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $code
     *
     * @ORM\Column(name="cod" type="string")
     */
    protected $code;

    /**
     * @var string $romanianName
     *
     * @ORM\Column(name="denr" type="string")
     */
    protected $romanianName;

    /**
     * @var string $englishName
     *
     * @ORM\Column(name="dene" type="string")
     */
    protected $englishName;

    /**
     * @var Repartition[] $repartitions
     *
     * @ORM\ManyToOne(targetEntity="Repartition", inversedBy="discipline")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $repartitions;

    /**
     * @param int    $id
     * @param string $code
     * @param string $romanianName
     * @param string $englishName
     */
    public function __construct($id, $code, $romanianName, $englishName)
    {
        $this->id = $id;
        $this->code = $code;
        $this->romanianName = $romanianName;
        $this->englishName = $englishName;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getRomanianName()
    {
        return $this->romanianName;
    }

    /**
     * @param string $romanianName
     */
    public function setRomanianName($romanianName)
    {
        $this->romanianName = $romanianName;
    }

    /**
     * @return string
     */
    public function getEnglishName()
    {
        return $this->englishName;
    }

    /**
     * @param string $englishName
     */
    public function setEnglishName($englishName)
    {
        $this->englishName = $englishName;
    }


    /**
     * @return Repartition[]
     */
    public function getRepartitions()
    {
        return $this->repartitions;
    }

    /**
     * @param Repartition[] $repartitions
     */
    public function setRepartitions($repartitions)
    {
        $this->repartitions = $repartitions;
    }

}
