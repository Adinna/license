<?php

namespace UBB\ScheduleBundle\Entities;

use Doctrine\ORM\Mapping as ORM;
use UBB\BuildingBundle\Entities\Building\Room;
use UBB\DisciplineBundle\Entities\Discipline;
use UBB\PersonnelBundle\Entities\Teacher;
use UBB\SpecializationBundle\Entities\Specialization\Group;

/**
 * Class Repartition
 * @package UBB\ScheduleBundle\Entities
 *
 * @ORM\Table(name="repart")
 */
class Repartition
{
    /**
     * @var int $id;
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Discipline $discipline
     *
     * @ORM\OneToMany(targetEntity="Discipline", mappedBy="repartitions")
     * @ORM\Column(name="disciplina" type="string")
     */
    protected $discipline;

    /**
     * @var string $activityType
     *
     * @ORM\Column(name="tipactiv" type="string")
     */
    protected $activityType;

    /**
     * @var int $numberOfHours
     *
     * @ORM\Column(name="nrore" type="integer")
     */
    protected $numberOfHours;

    /**
     * @var Group $group
     *
     * @ORM\OneToMany(targetEntity="Group", mappedBy="repartitions")
     * @ORM\Column(name="formatia" type="string")
     */
    protected $group;

    /**
     * @var int $semester
     *
     * @ORM\Column(name="sem" type="integer")
     */
    protected $semester;

    /**
     * @var int $groupedWith
     *
     * @ORM\Column(name="cuplat" type="integer")
     */
    protected $groupedWith;

    /**
     * @var int $repartitionType
     *
     * @ORM\Column(name="tiprep" type="integer")
     */
    protected $repartitionType;

    /**
     * @var Teacher $teacher
     *
     * @ORM\OneToMany(targetEntity="Teacher", mappedBy="repartitions")
     * @ORM\Column(name="persoana" type="string")
     *
     */
    protected $teacher;

    /**
     * @var Room $room
     *
     * @ORM\OneToMany(targetEntity="Room", mappedBy="repartitions")
     * @ORM\Column(name="sala" type="string")
     */
    protected $room;

    /**
     * @var Day $day
     *
     * @ORM\OneToMany(targetEntity="Day", mappedBy="repartitions")
     * @ORM\Column(name="zi" type="string")
     */
    protected $day;

    /**
     * @var Hour $beginHour
     *
     * @ORM\OneToMany(targetEntity="Hour", mappedBy="repartitions")
     * @ORM\Column(name="ora_i" type="integer")
     */
    protected $beginHour;

    /**
     * @var Hour $endHour
     *
     * @ORM\OneToMany(targetEntity="Hour", mappedBy="repartitions")
     * @ORM\Column(name="ora_s" type="integer")
     */
    protected $endHour;

    /**
     * @var int $projectors
     *
     * @ORM\Column(name="proiectoare" type="integer")
     */
    protected $projectors;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Discipline
     */
    public function getDiscipline()
    {
        return $this->discipline;
    }

    /**
     * @param Discipline $discipline
     */
    public function setDiscipline($discipline)
    {
        $this->discipline = $discipline;
    }

    /**
     * @return string
     */
    public function getActivityType()
    {
        return $this->activityType;
    }

    /**
     * @param string $activityType
     */
    public function setActivityType($activityType)
    {
        $this->activityType = $activityType;
    }

    /**
     * @return int
     */
    public function getNumberOfHours()
    {
        return $this->numberOfHours;
    }

    /**
     * @param int $numberOfHours
     */
    public function setNumberOfHours($numberOfHours)
    {
        $this->numberOfHours = $numberOfHours;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return int
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * @param int $semester
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;
    }

    /**
     * @return int
     */
    public function getGroupedWith()
    {
        return $this->groupedWith;
    }

    /**
     * @param int $groupedWith
     */
    public function setGroupedWith($groupedWith)
    {
        $this->groupedWith = $groupedWith;
    }

    /**
     * @return int
     */
    public function getRepartitionType()
    {
        return $this->repartitionType;
    }

    /**
     * @param int $repartitionType
     */
    public function setRepartitionType($repartitionType)
    {
        $this->repartitionType = $repartitionType;
    }

    /**
     * @return Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @param Teacher $teacher
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * @return Room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param Room $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * @return Day
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param Day $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return Hour
     */
    public function getBeginHour()
    {
        return $this->beginHour;
    }

    /**
     * @param Hour $beginHour
     */
    public function setBeginHour($beginHour)
    {
        $this->beginHour = $beginHour;
    }

    /**
     * @return Hour
     */
    public function getEndHour()
    {
        return $this->endHour;
    }

    /**
     * @param Hour $endHour
     */
    public function setEndHour($endHour)
    {
        $this->endHour = $endHour;
    }

    /**
     * @return int
     */
    public function getProjectors()
    {
        return $this->projectors;
    }

    /**
     * @param int $projectors
     */
    public function setProjectors($projectors)
    {
        $this->projectors = $projectors;
    }
}
