<?php

namespace UBB\ScheduleBundle\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Hour
 * @package UBB\ScheduleBundle\Entities

 * @ORM\Table(name="ore")
 */
class Hour
{
    /**
     * @var int $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Hour[] $beginHour
     *
     * @ORM\Column(name="ora_i" type="integer")
     * @ORM\ManyToOne(targetEntity="Repartition", inversedBy="beginHour")
     */
    protected $beginHour;

    /**
     * @var Hour[] $endHour
     *
     * @ORM\Column(name="ora_s" type="integer")
     * @ORM\ManyToOne(targetEntity="Repartition", inversedBy="endHour")
     */
    protected $endHour;

    /**
     * @param int    $id
     * @param string $beginHour
     * @param string $endHour
     */
    public function __construct($id, $beginHour, $endHour)
    {
        $this->id = $id;
        $this->beginHour = $beginHour;
        $this->endHour = $endHour;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Hour[]
     */
    public function getBeginHour()
    {
        return $this->beginHour;
    }

    /**
     * @param Hour[] $beginHour
     */
    public function setBeginHour($beginHour)
    {
        $this->beginHour = $beginHour;
    }

    /**
     * @return Hour[]
     */
    public function getEndHour()
    {
        return $this->endHour;
    }

    /**
     * @param Hour[] $endHour
     */
    public function setEndHour($endHour)
    {
        $this->endHour = $endHour;
    }
}
