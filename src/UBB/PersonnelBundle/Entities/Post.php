<?php

namespace UBB\PersonnelBundle\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 * @package UBB\PersonnelBundle\Entities
 *
 * @ORM\Table(name="posturi")
 */
class Post
{
    /**
     * @var int $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="nume" type="string")
     */
    protected $name;

    /**
     * @var string $romanianName
     *
     * @ORM\Column(name="denr" type="string")
     */
    protected $romanianName;

    /**
     * @var string $englishName
     *
     * @ORM\Column(name="dene" type="string")
     */
    protected $englishName;

    /**
     * @var int $rank
     *
     * @ORM\Column(name="pri" type="integer")
     */
    protected $rank;

    /**
     * @var Teacher[] $teachers
     *
     * @ORM\ManyToOne(targetEntity="Teacher", inversedBy="post")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $teachers;

    /**
     * @param int    $id
     * @param string $name
     * @param string $romanianName
     * @param string $englishName
     * @param int    $rank
     */
    public function __construct($id, $name, $romanianName, $englishName, $rank)
    {
        $this->id = $id;
        $this->name = $name;
        $this->romanianName = $romanianName;
        $this->englishName = $englishName;
        $this->rank = $rank;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getRomanianName()
    {
        return $this->romanianName;
    }

    /**
     * @param string $romanianName
     */
    public function setRomanianName($romanianName)
    {
        $this->romanianName = $romanianName;
    }

    /**
     * @return string
     */
    public function getEnglishName()
    {
        return $this->englishName;
    }

    /**
     * @param string $englishName
     */
    public function setEnglishName($englishName)
    {
        $this->englishName = $englishName;
    }

    /**
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return Teacher[]
     */
    public function getTeachers()
    {
        return $this->teachers;
    }

    /**
     * @param Teacher[] $teachers
     */
    public function setTeachers($teachers)
    {
        $this->teachers = $teachers;
    }
}
