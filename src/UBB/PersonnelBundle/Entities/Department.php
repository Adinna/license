<?php

namespace UBB\PersonnelBundle\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Department
 * @package UBB\PersonnelBundle\Entities
 *
 * @ORM\Table(name="catedre")
 */
class Department
{
    /**
     * @var int $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $romanianName
     *
     * @ORM\Column(name="denr" type="string")
     */
    protected $romanianName;

    /**
     * @var string $englishName
     *
     * @ORM\Column(name="dene" type="string")
     */
    protected $englishName;

    /**
     * @var Teacher[] $teachers
     *
     * @ORM\ManyToOne(targetEntity="Teacher", inversedBy="department")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $teachers;

    /**
     * @param int    $id
     * @param string $romanianName
     * @param string $englishName
     */
    public function __construct($id, $romanianName, $englishName)
    {
        $this->id = $id;
        $this->romanianName = $romanianName;
        $this->englishName = $englishName;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRomanianName()
    {
        return $this->romanianName;
    }

    /**
     * @param string $romanianName
     */
    public function setRomanianName($romanianName)
    {
        $this->romanianName = $romanianName;
    }

    /**
     * @return string
     */
    public function getEnglishName()
    {
        return $this->englishName;
    }

    /**
     * @param string $englishName
     */
    public function setEnglishName($englishName)
    {
        $this->englishName = $englishName;
    }

    /**
     * @return Teacher[]
     */
    public function getTeachers()
    {
        return $this->teachers;
    }

    /**
     * @param Teacher[] $teachers
     */
    public function setTeachers($teachers)
    {
        $this->teachers = $teachers;
    }
}
