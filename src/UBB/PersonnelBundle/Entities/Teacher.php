<?php

namespace UBB\PersonnelBundle\Entities;

use Doctrine\ORM\Mapping as ORM;
use UBB\ScheduleBundle\Entities\Repartition;

/**
 * Class Teacher
 * @package UBB\PersonnelBundle\Entities
 *
 * @ORM\Table(name="cadre")
 */
class Teacher
{
    /**
     * @var int $id;
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="nume" type="string")
     */
    protected $name;

    /**
     * @var Department $department
     *
     * @ORM\Column(name="catedra" type="integer")
     * @ORM\OneToMany(targetEntity="Department", mappedBy="teachers")
     */
    protected $department;

    /**
     * @var Post $post
     *
     * @ORM\Column(name="post" type="integer")
     * @ORM\OneToMany(targetEntity="Post", mappedBy="teachers")
     */
    protected $post;

    /**
     * @var int $doctoral
     *
     * @ORM\Column(name="dr" type="integer")
     */
    protected $doctoral;

    /**
     * @var int $active
     *
     * @ORM\Column(name="activ" type="integer")
     */
    protected $active;

    /**
     * @var string $code
     *
     * @ORM\Column(name="cod" type="string")
     */
    protected $code;

    /**
     * @var $string $address
     *
     * @ORM\Column(name="adresa" type="string")
     */
    protected $address;

    /**
     * @var string $phoneNumber
     *
     * @ORM\Column(name="tel" type="string")
     */
    protected $phoneNumber;

    /**
     * @var string $restrictions
     *
     * @ORM\Column(name="restrictii" type="string")
     */
    protected $restrictions;

    /**
     * @var Repartition[] $repartitions
     *
     * @ORM\ManyToOne(targetEntity="Repartition", inversedBy="teacher")
     */
    protected $repartitions;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Department $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param Post $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    /**
     * @return int
     */
    public function getDoctoral()
    {
        return $this->doctoral;
    }

    /**
     * @param int $doctoral
     */
    public function setDoctoral($doctoral)
    {
        $this->doctoral = $doctoral;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getRestrictions()
    {
        return $this->restrictions;
    }

    /**
     * @param string $restrictions
     */
    public function setRestrictions($restrictions)
    {
        $this->restrictions = $restrictions;
    }
}
