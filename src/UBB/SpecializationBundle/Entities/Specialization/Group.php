<?php

namespace UBB\SpecializationBundle\Entities\Specialization;

use Doctrine\ORM\Mapping as ORM;
use UBB\ScheduleBundle\Entities\Repartition;
use UBB\SpecializationBundle\Entities\Specialization;

/**
 * Class Group
 * @package UBB\SpecializationBundle\Entities\Specialization
 *
 * @ORM\Table(name="formatii")
 */
class Group
{
    /**
     * @var int $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int $department
     *
     * @ORM\Column(name="sectia" type="integer")
     */
    protected $department;

    /**
     * @var Specialization $specialization
     *
     * @ORM\OneToMany(targetEntity="Specialization", mappedBy="groups")
     * @ORM\Column(name="an" type="integer")
     */
    protected $specialization;

    /**
     * @var string $code
     *
     * @ORM\Column(name="cod" type="string")
     */
    protected $code;

    /**
     * @var string $name
     *
     * @ORM\Column(name="denumire" type="string")
     */
    protected $name;

    /**
     * @var int $level
     *
     * @ORM\Column(name="nivel" type="integer")
     */
    protected $level;

    /**
     * @var string $component
     *
     * @ORM\Column(name="componenta" type="string")
     */
    protected $component;

    /**
     * @var Repartition[] $repartition
     *
     * @ORM\ManyToOne(targetEntity="Repartition", inversedBy="group")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $repartition;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param int $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return Specialization
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * @param Specialization $specialization
     */
    public function setSpecialization($specialization)
    {
        $this->specialization = $specialization;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return string
     */
    public function getComponent()
    {
        return $this->component;
    }

    /**
     * @param string $component
     */
    public function setComponent($component)
    {
        $this->component = $component;
    }

    /**
     * @return Repartition[]
     */
    public function getRepartition()
    {
        return $this->repartition;
    }

    /**
     * @param Repartition[] $repartition
     */
    public function setRepartition($repartition)
    {
        $this->repartition = $repartition;
    }
}
