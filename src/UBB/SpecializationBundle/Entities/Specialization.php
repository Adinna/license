<?php

namespace UBB\SpecializationBundle\Entities;

use Doctrine\ORM\Mapping as ORM;
use UBB\SpecializationBundle\Entities\Specialization\Group;

/**
 * Class Specialization
 * @package UBB\SpecializationBundle\Entities
 *
 * @ORM\Table(name="ani")
 */
class Specialization
{
    /**
     * @var int $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int $year
     *
     * @ORM\Column(name="an" type="integer")
     */
    protected $year;

    /**
     * @var int $specification
     *
     * @ORM\Column(name="spec" type="integer")
     */
    protected $specification;

    /**
     * @var string $code
     *
     * @ORM\Column(name="cod" type="string")
     */
    protected $code;

    /**
     * @var Group[] $groups
     *
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="year")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $groups;

    /**
     * @param int    $id
     * @param int    $year
     * @param int    $specification
     * @param string $code
     */
    public function __construct($id, $year, $specification, $code)
    {
        $this->id = $id;
        $this->year = $year;
        $this->specification = $specification;
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getSpecification()
    {
        return $this->specification;
    }

    /**
     * @param int $specification
     */
    public function setSpecification($specification)
    {
        $this->specification = $specification;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return Group[]
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param Group[] $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }
}
