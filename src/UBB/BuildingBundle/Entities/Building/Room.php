<?php

namespace UBB\BuildingBundle\Entities\Building;

use UBB\BuildingBundle\Entities\Building;
use UBB\ScheduleBundle\Entities\Repartition;

/**
 * Class Room
 * @package UBB\BuildingBundle\Entities\Building
 *
 * @ORM\Table(name="sali")
 */
class Room
{
    /**
     * $var int $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * $var string $code
     *
     * @ORM\Column(name="cod" type="string")
     */
    protected $code;

     /**
      * $var int $amountOfSeats
      *
      * @ORM\Column(name="locuri" type="integer")
      */
    protected $amountOfSeats;

    /**
     * $var string $legend
     *
     * @ORM\Column(name="legenda" type="string")
     */
    protected $legend;

    /**
     * $var Building $building
     *
     * @ORM\OneToMany(targetEntity="Building", mappedBy="rooms")
     * @ORM\Column(name="cladire" type="integer")
     */
    protected $building;

    /**
     * $var int $status
     *
     * @ORM\Column(name="status" type="integer")
     */
    protected $status;

    /**
     * $var string $restrictions
     *
     * @ORM\Column(name="restrictii" type="string")
     */
    protected $restrictions;

    /**
     * @var Repartition[] $repartitions
     *
     * @ORM\ManyToOne(targetEntity="Repartition", inversedBy="room")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $repartitions;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getAmountOfSeats()
    {
        return $this->amountOfSeats;
    }

    /**
     * @param int $amountOfSeats
     */
    public function setAmountOfSeats($amountOfSeats)
    {
        $this->amountOfSeats = $amountOfSeats;
    }

    /**
     * @return string
     */
    public function getLegend()
    {
        return $this->legend;
    }

    /**
     * @param string $legend
     */
    public function setLegend($legend)
    {
        $this->legend = $legend;
    }

    /**
     * @return Building
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param Building $building
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getRestrictions()
    {
        return $this->restrictions;
    }

    /**
     * @param string $restrictions
     */
    public function setRestrictions($restrictions)
    {
        $this->restrictions = $restrictions;
    }
}
