<?php

namespace UBB\BuildingBundle\Entities;

use Doctrine\ORM\Mapping as ORM;
use UBB\BuildingBundle\Entities\Building\Room;

/**
 * Class Building
 * @package UBB\BuildingBundle\Entities
 *
 * @ORM\Table(name="cladiri")
 */
class Building
{
    /**
     * @var int $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $romanianName
     *
     * @ORM\Column(name="denr" type="string")
     */
    protected $romanianName;

    /**
     * @var string $englishName
     *
     * @ORM\Column(name="dene" type="string")
     */
    protected $englishName;

    /**
     * @var int $projectors
     *
     * @ORM\Column(name="proiectoare" type="integer")
     */
    protected $projectors;

    /**
     * @var Room[] $rooms
     *
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="building")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $rooms;

    /**
     * @param int    $id
     * @param string $romanianName
     * @param string $englishName
     * @param int    $projectors
     */
    public function __construct($id, $romanianName, $englishName, $projectors)
    {
        $this->id = $id;
        $this->romanianName = $romanianName;
        $this->englishName = $englishName;
        $this->projectors = $projectors;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRomanianName()
    {
        return $this->romanianName;
    }

    /**
     * @param string $romanianName
     */
    public function setRomanianName($romanianName)
    {
        $this->romanianName = $romanianName;
    }

    /**
     * @return string
     */
    public function getEnglishName()
    {
        return $this->englishName;
    }

    /**
     * @param string $englishName
     */
    public function setEnglishName($englishName)
    {
        $this->englishName = $englishName;
    }

    /**
     * @return int
     */
    public function getProjectors()
    {
        return $this->projectors;
    }

    /**
     * @param int $projectors
     */
    public function setProjectors($projectors)
    {
        $this->projectors = $projectors;
    }

    /**
     * @return Room[]
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param Room[] $rooms
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
    }
}
